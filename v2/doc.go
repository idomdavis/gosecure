// Package gosecure provides hashing, checking and associated functions for
// passphrases and JWT tokens. Tokens allow for session data to be passed in by
// a client having been encoded earlier. With both hashes and tokens secure
// cryptographic functions are used. All functions can be called in a time boxed
// fashion with guarantees about minimum execution time using the Timebox
// function.
package gosecure
